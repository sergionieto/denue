package com.freshmintsoft.inegimapas.test;

import com.freshmintsoft.inegimapas.service.JsonConverter;

import android.test.AndroidTestCase;

public class TestJsonConverter extends AndroidTestCase {

	JsonConverter jsonConverter;
	String json;
	ReadLocalJson readJson;
	
	public TestJsonConverter(){
		super();
	}
	
	public void setUp(){
		readJson = new ReadLocalJson(getContext());
	}
	
	public void testReadJson(){
		String myFile = readJson.readFile();
		assertNotNull(myFile);
	}
}

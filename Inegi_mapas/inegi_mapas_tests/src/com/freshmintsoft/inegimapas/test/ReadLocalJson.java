package com.freshmintsoft.inegimapas.test;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.freshmintsoft.inegimapas.R;

import android.content.Context;
import android.util.Log;

public class ReadLocalJson {
	
	private String jsonString;
	private Context context;
	private static final int DENUE_FILE = R.raw.denue;
	private static String TAG = "LOCAL_JSON_READ";
	
	public ReadLocalJson(Context context){
		this.context = context;
	}
	
	public String readFile(){
		InputStream fis = context.getResources().openRawResource(DENUE_FILE);
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String line;
		jsonString = "";
		try {
			while((line = br.readLine()) != null){
				jsonString += line;
			}
			fis.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonString;
	}
	
	public void printFileToConsole(){
		if(jsonString != null){
			Log.d(TAG, jsonString);
		}
	}
}

package com.freshmintsoft.inegimapas.test;


import com.freshmintsoft.inegimapas.service.FusionTablesHttpConnection;

import android.content.Context;
import android.test.AndroidTestCase;

public class TestFusionTablesConnection extends AndroidTestCase{

	FusionTablesHttpConnection connection;
	Context context;
	
	public TestFusionTablesConnection() {
		super();
	}
	
	
	public void testConnection(){
		context = this.getContext();
		
		connection = new FusionTablesHttpConnection();
		Thread t = new Thread(connection);
		t.start();
		while(connection.isActive) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		assertNotNull(context);
		assertNotNull(connection.contents);
	}
}

package com.freshmintsoft.inegimapas;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class DenuePreferences extends PreferenceActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}

	
}

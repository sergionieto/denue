package com.freshmintsoft.inegimapas.background;



import java.util.List;

import com.freshmintsoft.inegimapas.model.GPSUbicacion;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;

/**
 * Clase que se encarga de obtener la ubicacion del dispositivo movil
 * Se debe de invocar el metodo de <code>startGPS(context)</code>
 * para poder obtener el objeto: <code>GPSUbicacion</code>
 * y no olvidarse de cerrar el servicio con <code>removeLocationService()</code>
 * @author freshmintsoft
 * @see LocationListener
 *
 */
public class GPSTask implements LocationListener{

	private Context context;
	public Location deviceLocation;
	private LocationManager locationManager;
	private String provider;
	private GPSUbicacion gpsUbicacion;
	private Criteria criteria;
	private List<String> enabledProviders;
	
	private boolean isMultipleProvidersSelected = false;
	
	public GPSTask(Context context){
		this.context = context;
		
		initializeGpsWrapperObject();
		initializeLocationManager();
		getAvailableLocationProviders();
	}
	
	private void initializeGpsWrapperObject(){
		gpsUbicacion = new GPSUbicacion();
	}
	
	private void initializeLocationManager(){
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	}
	
	
	private void getAvailableLocationProviders(){
		specifyLocationProvidersCriteria();
		provider = locationManager.getBestProvider(criteria, true);
		if(provider == null) {
			specifyMultipleLocationProvidersCriteria();
			enabledProviders = locationManager.getProviders(criteria, true);
			isMultipleProvidersSelected = true;
		}
	}
	
	private void specifyLocationProvidersCriteria() {
		criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setPowerRequirement(Criteria.POWER_HIGH);
		//criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		//criteria.setSpeedRequired(false);
		//criteria.setCostAllowed(true);
	}
	
	private void specifyMultipleLocationProvidersCriteria(){
		criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_COARSE);
	}
	
	/**
	 * Inicia la busqueda de ubicacion del dispositivo.
	 * De no obtener un proveedor para obtener la ubicacion
	 * Envia al usuario para que habilita una de ellas.
	 */
	public void startGPS(){
		if(provider == null | provider == ""){
			openLocationSettings();
		} else {
			if(isMultipleProvidersSelected) {
				requestLocationFromMultipleProviders();
			}else {
				requestLocationFromProvider(provider);
			}
		}	
	}
	
	private void openLocationSettings(){
		context.startActivity((new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)));
	}
	
	private void requestLocationFromMultipleProviders(){
		for(String enabledProvider : enabledProviders) {
			locationManager.requestSingleUpdate(enabledProvider, this, null);
		}
	}
	
	private void requestLocationFromProvider(String provider){
		//locationManager.requestSingleUpdate(provider, this, null);
		
		locationManager.requestLocationUpdates(provider, 2000, 10, this);
	}
	
	/**
	 * Obtiene la ubicacion mas proxima del dispositivo.
	 * @return GPSUbicacion que representa la longitud y latitud.
	 * @see GPSUbicacion
	 */
	public GPSUbicacion getGPSUbicacion(){
		
		requestLasLocationUpdateFromProvider();
		
		return gpsUbicacion;
	}
	
	private boolean isGpsUbicacionEmpty(){
		if(gpsUbicacion == null){
			return true;
		}
		return false;
	}
	
	private void requestLasLocationUpdateFromProvider(){
		
		Location location = locationManager.getLastKnownLocation(provider);
		this.onLocationChanged(location);
	}
	
//Metodos de la interfaz LocationListener.	
	/**
	 * Metodo que se ejecuta cuando se detecta un cambio en la ubicacion
	 * del dispositivo.
	 */
	public void onLocationChanged(Location location) {
		if(location != null) {
			this.deviceLocation = location;
			gpsUbicacion.setLatitud(location.getLatitude());
			gpsUbicacion.setLongitud(location.getLongitude());
		}
	}
	
	/* Intentionally left blank as part of interface contract*/
	public void onProviderDisabled(String provider) {		}

	/* Intentionally left blank as part of interface contract*/
	public void onProviderEnabled(String provider) { }

	/* Intentionally left blank as part of interface contract*/
	public void onStatusChanged(String provider, int status, Bundle extras) { }
	
	public void removeLocationService(){
		locationManager.removeUpdates(this);
	}

	public void enableLocationUpdates(){
		startGPS();
	}
}

package com.freshmintsoft.inegimapas;

import java.util.List;
import java.util.Map;

import com.freshmintsoft.inegimapas.background.GPSTask;
import com.freshmintsoft.inegimapas.model.GPSUbicacion;
import com.freshmintsoft.inegimapas.model.InegiLocal;
import com.freshmintsoft.inegimapas.service.DenueQueryServiceInterface;
import com.freshmintsoft.inegimapas.service.XmlDenueQueryService;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.Location;
import android.preference.PreferenceManager;

/**
 * Aplicacion principal del DENUE
 * 
 * Guarda referencia a la lista de resultados
 * obtenida por el SOAP Service.
 * Setea el metodo de respuesta del SOAP Service.
 * Inicializa el GPS del dispositivo.
 * @author freshmintsoft
 * @see XmlDenueQueryService
 * @See InegiLocal
 *
 */
public class DenueApplication extends Application implements OnSharedPreferenceChangeListener{
	
	private List<InegiLocal> resultados;
	
	/** Ubicacion del dispositivo **/
	public Location location;
	/** Valor de las preferencias para obtener resultados **/
	public int rangoDatos = 10;
	/** Limite inferior para obtener los resultados del servidor **/
	public int inicio = 0;
	/** Limite superior para obtener resultados del servidor **/
	public int topeT;
	/** Bandera para saber si la busqueda sigue siendo la misma **/
	public boolean isTheSameQuery = false;
	
	public boolean enableStreetView = true;
	
	/** Valor que representa el radio de b�squeda del gps **/
	public int gpsRango = 500;
	
	/** cartografia del inegi **/
	private Map<String, List<GPSUbicacion>> areaMap;
	
	/** Bandera para que solo se pinte una vez la cartografia **/
	public boolean isMappingPainted = false;
	
	/** Representa el estado que se esta seleccionando **/
	public int localidad = 0;
	/** Representa el municipio que se esta seleccionando **/
	public int municipio = 0;
	
	/** Representa el array de municipios Seleccionado por el estado **/
	public String[] municipiosArray;
	
	private SharedPreferences denuePreferences;
	private static final String PREFERENCE_RANGO_KEY = "PREF_RANGO";
	private GPSTask gpsTask;
	
	/** Indice seleccionado de la lista de ResultadosListActivity **/
	public int detalleIndexSelected = -1;
	
	/** Overlay Seleccionado cuando se da un tap sobre un icono del mapa **/
	public int overlayIndexSelected = -1;
	
	//Prefs keys
	private static final String RANGO_KEY = "PREF_RANGO";
	private static final String GPS_KEY = "PREF_GPS";
	@Override
	public void onCreate() {
		super.onCreate();
		initializeGPS();
		managePreferences();
	}

	
	@Override
	public void onTerminate() {
		gpsTask.removeLocationService();
		super.onTerminate();
	}
	
	public void removeLocationServiceUpdates(){
		gpsTask.removeLocationService();
	}
	
	public void enableLocationUpdates(){
		
	}
	
	private void initializeGPS(){
		gpsTask = new GPSTask(this);
		gpsTask.startGPS();
		location = gpsTask.deviceLocation;
	}
	
	private void managePreferences() {
		getApplicationPreferences();
		registerPreferenceChanges();
		String value = getStartedPreferenceRangoValue();
		persistDefaultPreferenceValue(value);
	}
	
	private void persistDefaultPreferenceValue(String value) {
		rangoDatos = Integer.parseInt(value);
		topeT = rangoDatos;
	}

	/**
	 * Obtiene la ubicacion actual del dispositvo.
	 * @return GPSUbicacion que representa la longitud y la latitud.
	 * @see GPSUbicacion
	 * @see GPSTask
	 */
	public GPSUbicacion getUbicacion(){
		return gpsTask.getGPSUbicacion();
	}
	
	/** Setea la lista de resultados, obtenida por el web service */
	public void setDenueResultados(List<InegiLocal> resultados){
		this.resultados = resultados;
	}
	/** Obtiene la lista de resultados del servidor del DENUE */
	public List<InegiLocal> getDenueResultados(){
		return resultados;
	}
	
	/** Obtiene la referencia al metodo de comunicacion con el servicio del DENUE */
	public DenueQueryServiceInterface getRespuestaInegiService(){
		return new XmlDenueQueryService(this);
	}

	/** Obtiene el mapa de la cartografia a Pintar **/
	public Map<String, List<GPSUbicacion>> getAreaMap() {
		return areaMap;
	}

	/** 
	 * Setea la cartografia del inegi a pintar
	 * @see PolygonOverlay
	 * @see FusionTablesConnection 
	 */
	public void setAreaMap(Map<String, List<GPSUbicacion>> areaMap) {
		this.areaMap = areaMap;
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if(key.equals(PREFERENCE_RANGO_KEY)) {
			String value = prefs.getString(key, "10");
			rangoDatos = Integer.parseInt(value);
			topeT = rangoDatos;
		} else if (key.equals(GPS_KEY)) {
			String value = prefs.getString(key, "500");
			gpsRango = Integer.parseInt(value);
		}
	}
	
	private void getApplicationPreferences(){
		denuePreferences = PreferenceManager.getDefaultSharedPreferences(this);
		
	}
	
	private void registerPreferenceChanges(){
		denuePreferences.registerOnSharedPreferenceChangeListener(this);
	}
	
	private String getStartedPreferenceRangoValue(){
		String value = denuePreferences.getString(PREFERENCE_RANGO_KEY, "10");
		return value;
	}
	
	public String getRangoPreference(){
		return Integer.toString(rangoDatos);
	}
}

package com.freshmintsoft.inegimapas;



import java.util.List;

import com.freshmintsoft.inegimapas.model.InegiLocal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ResultadosListActivity extends ListActivity {

	ArrayAdapter<InegiLocal> aa;
	List<InegiLocal> resultados;
	Dialog dialog;
	DenueApplication application;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		application = (DenueApplication) getApplication();
		getResultados();
		aa = new ArrayAdapter<InegiLocal>(getBaseContext(), 
				android.R.layout.simple_list_item_1, 
				resultados);
		setListAdapter(aa);
		
	}
	
	private void getResultados(){
		DenueApplication app = (DenueApplication)getApplication();
		resultados = app.getDenueResultados();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	private void CreateMenu(Menu menu){
		MenuItem preferences = menu.add(0,0,0, "Preferencias");
		{
			//preferences.setIcon(R.drawable.menu_mapa);
			//mapDisplay.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		return MenuChoice(item);
	}

	private boolean MenuChoice(MenuItem item){
		switch(item.getItemId()){
			case 0:
				Intent preference = new Intent(this, DenuePreferences.class);
				startActivity(preference);
				return true;
		}
		return false;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		showDialog(position);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		application.detalleIndexSelected = id;
		ResultadosDetalleActivity builder = new ResultadosDetalleActivity(this, id);
		builder.initializeComponents();
		InegiLocal local = resultados.get(id);
		builder.setDataToComponents(local);
		dialog = builder.create();
		return builder.create();
	}

	
	@Override
	protected void onPause() {
		if(dialog != null) {
			dialog.dismiss();
		}
		super.onStop();
	}

	public void addNewResultado(InegiLocal resultado){
		resultados.add(resultado);
		aa.notifyDataSetChanged();
	}
}

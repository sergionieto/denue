package com.freshmintsoft.inegimapas.maputils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Paint.Style;


import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

/**
 * Marca que representa al dispositivo actual en el 
 * mapa de Google Maps.
 * Crea un circulo rojo para representar al dispositivo
 * en el metodo <code>draw</code>
 * @author freshmintsoft
 * @see Overlay
 */
public class ExpandedRadiusDeviceOverlay extends MyLocationOverlay
{
	GeoPoint geoPoint;
	String name;
	MapView mapView;
	
	private Paint paintFill;
	private Paint paintStroke;
	private static final int ALPHA = 0x30ffffff;
	private static final int COLOR = Color.MAGENTA & ALPHA;
	int rango;
	
	public static int metersToRadius(float meters, MapView map, double latitude) {
	    return (int) (map.getProjection().metersToEquatorPixels(meters) * (1 / Math
	            .cos(Math.toRadians(latitude))));
	}
	
	public ExpandedRadiusDeviceOverlay(Context context, MapView mapView, int rango){
		super(context, mapView);
		this.rango = rango;
		prepareCircleAroundLocation();
	}

	private void prepareCircleAroundLocation(){
		paintFill = new Paint();
		paintFill.setStyle(Paint.Style.FILL);
		paintFill.setColor(COLOR);
		
		paintStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintStroke.setStyle(Style.STROKE);
		paintStroke.setAntiAlias(true);
		paintStroke.setStrokeWidth(3);
		paintStroke.setColor(COLOR & 0xff7f7f7f);
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapview, boolean shadow) {
		Projection projection = mapView.getProjection();
		if(shadow == false){
			Point myPoint = new Point();
			projection.toPixels(this.geoPoint, myPoint);
			
			//int radius = (int) mapView.getProjection().metersToEquatorPixels(rango);
			RectF oval = new RectF(myPoint.x - rango, myPoint.y - rango, 
									myPoint.x + rango, myPoint.y + rango);
			
			canvas.drawOval(oval, paintFill);
			canvas.drawText("Radio de Busqueda: " + rango, myPoint.x + rango, myPoint.y, paintFill);
		}
	}
}
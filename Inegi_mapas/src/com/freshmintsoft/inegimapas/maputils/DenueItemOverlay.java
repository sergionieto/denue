package com.freshmintsoft.inegimapas.maputils;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;

import com.freshmintsoft.inegimapas.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;

/**
 * Representa la lista de overlays o marcas que apareceran en el mapa
 * de google maps, representando cada item de la lista obtenida
 * por el Denue Service.
 * @author freshmintsoft
 *
 *@see ItemizedOverlay
 *@see DenueItem
 */
public class DenueItemOverlay extends ItemizedOverlay<DenueItem>{

	private ArrayList<DenueItem> items;
	ItemOverlayListener listener;
	Context context;
	
	public DenueItemOverlay(Drawable markerImage, Context context){
		super(boundCenterBottom(markerImage));
		listener = (ItemOverlayListener) context;
		items = new ArrayList<DenueItem>();
		this.context = context;
		populate();
	}
	
	
	/**
	 * Inserta un nuevo item a la lista de marcadores.
	 * @param id ID del componente.
	 * @param location ubicacion del componente
	 * @param markerText Texto a desplegar del componente.
	 * @param snippet fragmento a mostrar.
	 */
	public void addNewItem(Integer id, GeoPoint location, String markerText, String snippet){
		DenueItem itemOverlay = new DenueItem(id, location, markerText, snippet);
		items.add(itemOverlay);
		populate();
	}
	
	public void changeMarkerToBlue(int index){
		DenueItem item = this.getItem(index);
		Drawable blueIcon = this.boundCenterBottom(
					context.getResources().getDrawable(R.drawable.blue_map_overlay) );
		item.setMarker(blueIcon);
		populate();
	}
	/**
	 * Elimina un item de la lista de marcadores.
	 * @param index el indice del item a eliminar.
	 */
	public void removeItem(int index){
		items.remove(index);
		populate();
	}
	
	@Override
	protected DenueItem createItem(int arg0) {
		return items.get(arg0);
	}

	@Override
	public int size() {
		return items.size();
	}

	
	@Override
	public boolean onSnapToItem(int arg0, int arg1, Point arg2, MapView arg3) {
		return false;
	}
	
	@Override
	public boolean onTap(int index) {
		DenueItem item = items.get(index);
		if(item != null) {
			listener.onOverlayClicked(index);
			//showDetails(index);
			
		}

		return true;
	}
	
	
}
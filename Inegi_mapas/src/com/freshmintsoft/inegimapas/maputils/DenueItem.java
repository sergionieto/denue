package com.freshmintsoft.inegimapas.maputils;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

/**
 * Extiende la funcionalidad del OverlayItem para poder
 * setear el id obtenido por el DenueService
 * @author freshmintsoft
 * @see OverlayItem
 */
public class DenueItem extends OverlayItem{
	private Integer id;
	
	public DenueItem(Integer id, GeoPoint location, String markerText, String snippet){
		super(location,markerText, snippet);
		this.id = id;
	}
	
	public Integer getId(){
		return id;
	}
}

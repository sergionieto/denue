package com.freshmintsoft.inegimapas.service;



import android.app.Activity;
import android.content.Context;

import android.util.Log;

import com.freshmintsoft.inegimapas.DenueApplication;
import com.freshmintsoft.inegimapas.R;
import com.freshmintsoft.inegimapas.RespuestaInegiListener;


import com.freshmintsoft.inegimapas.model.GPSUbicacion;


/**
 * Representacion en XML de la interfaz de DenueQueryServiceInterface.
 * 
 * La representacion se basa en que la respuesta obtenida es por 
 * medio de SOAP y XML.
 * @author freshmintsoft
 *
 */
public class XmlDenueQueryService implements DenueQueryServiceInterface {
	
	private Context context;
	private static String TAG = "Busqueda Task";

	private DenueApplication application;
	private RespuestaInegiListener respuestaListener;
	
	public XmlDenueQueryService(Context context){
		this.context = context; 
		application = (DenueApplication) context.getApplicationContext();
	}
	
	/**
	 * Hace un query al servidor en base a la ubicacion actual del dispositivo
	 * Obtiene los 100 resultados mas cercanos.
	 */
	public void getDataBasedOnGps() {
		// TODO Auto-generated method stub
		GPSUbicacion current = getUbicacion();

	}

	/**
	 * Hace un query al servidor en base a una busqueda (Categoria o servicio)
	 * de acuerdo a una entidad o municipio seleccionado.
	 */
	public void queryServer(String query, String entidadOMunicipio) {
		
		String request = constructRequestToSend(query);
		connectAndReceiveResponse(request);
		
	}

	/**
	 * Hace un query al servidor en base a una busqueda (Categoria o servicio)
	 * de acuerdo a la ubicacion del dispositivo.
	 */
	public void queryServerWithDeviceLocation(String query) {
		// TODO Auto-generated method stub

	}
	
	private GPSUbicacion getUbicacion(){
		GPSUbicacion current = application.getUbicacion();
		Log.d(TAG, "La ubicacion actual es: " + current.getLatitud() + ", " + current.getLongitud());
		return current;
	}
	
	private String constructRequestToSend(String query){
		DenueSoapRequestReader soapReader = new DenueSoapRequestReader(R.raw.soap_in, context);
		soapReader.readInFile();
		String inicio = getInicio();
		String topeT = getTopeT();
		String request = soapReader.setDenueParamsAndReturnAsAString(query, inicio, topeT);
		
		return request;
	}
	
	private String getInicio(){
		if(application.isTheSameQuery) {
			application.inicio = application.rangoDatos + application.inicio;
			return Integer.toString(application.inicio + 1);
		} else {
			return Integer.toString(application.inicio);
		}
	}
	
	private String getTopeT(){
		if(application.isTheSameQuery) {
			application.topeT = application.rangoDatos + application.topeT;
			
		} 
		return Integer.toString(application.topeT);
	}
	
	private void connectAndReceiveResponse(String request){
		DenueConnectionService soapService = new DenueConnectionService(request, this);
		Thread t = new Thread(soapService);
		t.start();
	}
	
	/**
	 * Obtiene referencia al contexto de la aplicacion principal.
	 */
	public DenueApplication getApplication(){
		return application;
	}

	/**
	 * Crea la referencia de la pantalla invocadora de este servicio.
	 */
	public void setCallView(Activity activity) {
		respuestaListener = (RespuestaInegiListener) activity;
	}
	
	/**
	 * Obtiene la referencia de la pantalla invocadora del servicio.
	 */
	public RespuestaInegiListener getListener(){
		return respuestaListener;
	}
}

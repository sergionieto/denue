package com.freshmintsoft.inegimapas.service;


import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import com.freshmintsoft.inegimapas.model.InegiLocal;


import android.os.Handler;
import android.util.Log;

/**
 * Se encarga de la conexion con el servicio SOAP del denue,
 * contiene las propiedades necesarias para conectarse.
 * 
 * Debido a que Android no permite hacer llamadas a la red en el ui thread,
 * La clase implementa Runnable para ejecutarse dentro de un thread.
 * 
 * @author freshmintsoft
 *
 */
public class DenueConnectionService implements Runnable{

	//SOAP
	private String url = "http://www5.inegi.org.mx/sistemas/mapa/Denue/webservice1.asmx?op=consultaSP";
	private String action = "http://tempuri.org/consultaSP";
	private String requestMethod = "POST";
	private String property = "Content-type";
	private String requestType = "text/xml; charset=utf-8";
	private String TAG = "DENUEConnectionService";
	
	private Handler handler = new Handler();
	private boolean isActive = true;
	
	private DenueQueryServiceInterface caller;
	private volatile InputStream responseStream;
	private List<InegiLocal> responseAsList = null;
	private String request;
	private HttpURLConnection con;
	
	public DenueConnectionService(String url, String action, String request, DenueQueryServiceInterface respuestaInegi){
		this.url = url;
		this.action = action;
		this.request = request;
		this.caller = respuestaInegi;
	}
	
	public DenueConnectionService(String request, DenueQueryServiceInterface respuestaInegi){
		this.request = request;
		this.caller = respuestaInegi;
	}
	
	public void run(){
		connectToService();
	}
	
	/**
	 * Se conecta al servidor del denue que responde a peticiones SOAP 1.1
	 * Se forma el request y se obtiene el resultado para posteriormente
	 * convertirlo en una lista de objetos. Misma que se guarda en el 
	 * <code>DenueApplication</code>
	 * @see DenueApplication
	 */
	public void connectToService(){
		try {
			setSoapConnection();
			try {	
				postSoapRequest();
				readResponseFromServer();	
			} catch (IOException e) {
				this.postResponseInUIThread();
				Log.d(TAG,"Cant read response");
			}
		} catch (Exception e) {
			Log.d(TAG,"URL Malformed");
		}
	}
	
	private void setSoapConnection() throws Exception{
		URL oUrl = new URL(url);
		con = (HttpURLConnection) oUrl.openConnection();
		con.setRequestMethod(requestMethod);
		con.setRequestProperty(property, requestType);
		con.setRequestProperty("SOAPAction", action);
		con.setDoOutput(true);
		con.setDoInput(true);
	}
	
	private void postSoapRequest()throws IOException{
		OutputStream reqStream = con.getOutputStream();
		
		reqStream.write(request.getBytes());
		reqStream.flush();
	}
	
	private void readResponseFromServer()throws IOException{
		responseStream = con.getInputStream();
		unmarshallResponse(responseStream);
		isActive = false;
	}
	
	/**
	 * Bandera usada para determinar si el servicio
	 * sigue conectado al servidor del denue
	 * @return true si esta conectado false si no lo esta.
	 */
	public boolean getIsActive(){
		return isActive;
	}
	
	private void unmarshallResponse(InputStream responseStream) {
		DenuSoapResponseUnMarshaller unMarshaller = new DenuSoapResponseUnMarshaller();
		try {
			responseAsList = unMarshaller.unMarshallResponse(responseStream);
			this.postResponseInUIThread();
		} catch (IOException e) {
			Log.e(TAG, "No se ha podido convertir el XML a Objeto");
		}
	}
	
	private void postResponseInUIThread(){
		handler.post(new Runnable() {
			@Override
			public void run() {
				caller.getApplication().setDenueResultados(responseAsList);
				caller.getListener().onRespuestaReceived();
			}
		});
	}
	
	/**
	 * Obtiene el flujo de bits de respuesta de la conexion.
	 * @return InputStream que representa la respuesta del server.
	 */
	public synchronized InputStream getResponseStream(){
		return responseStream;
	}
}

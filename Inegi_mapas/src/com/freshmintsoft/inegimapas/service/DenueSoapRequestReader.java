package com.freshmintsoft.inegimapas.service;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.util.Log;

/**
 * Obtiene referencia al archivo request que se utiliza en el SOAP
 * del DENUE.
 * @author freshmintsoft
 *
 */
public class DenueSoapRequestReader {

	private int fileId;
	private Context context;
	public InputStream inputSreamFile;
	private BufferedReader bufferedReader;
	private String TAG = "SoapFileReader";
	
	public DenueSoapRequestReader(int fileId, Context context){
		this.fileId = fileId;
		this.context = context;
	}
	
	/**
	 * Lee el archivo que se paso de referencia en el constructor.
	 * @return BufferedReader representando al archivo.
	 */
	public BufferedReader readInFile(){
		InputStream fis = context.getResources().openRawResource(fileId);
		inputSreamFile = fis;
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
		bufferedReader = reader;
		return reader;
	}
	
	/**
	 * Coloca los parametros necesarios para formar el request del SOAP service
	 * del denue, este request acepta tres parametros, la condicion, el inicio y 
	 * el fin de obtencion de datos.
	 * @param condicion la condicion de busqueda.
	 * @param inicio el inicio para obtener la informacion
	 * @param fin el limite para obtener la informacion.
	 * @return String que representa al archivo de request ya con la condicion, inicio y fin.
	 */
	public String setDenueParamsAndReturnAsAString(String condicion, String inicio, String fin){
		String line = "";
		String response = "";
		try {
			while( (line = bufferedReader.readLine())!= null) {
				if( line.contains("#condicion#")) {
					line = line.replace("#condicion#", condicion);
				} else if(line.contains("#inic#")){
					line = line.replace("#inic#", inicio);
				}else if(line.contains("#topeT#")) {
					line = line.replace("#topeT#", fin);
				}
				response += line;
			}
		} catch (IOException e) {
			Log.e(TAG, "No se pudo leer el archivo linea por linea");
		}
		return response;
	}
}

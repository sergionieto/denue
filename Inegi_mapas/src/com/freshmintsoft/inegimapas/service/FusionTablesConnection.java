package com.freshmintsoft.inegimapas.service;



import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.security.GeneralSecurityException;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;

import com.freshmintsoft.inegimapas.R;
/*import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.services.GoogleKeyInitializer;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpRequest;
import com.google.api.client.http.json.JsonHttpRequestInitializer;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.fusiontables.Fusiontables;
import com.google.api.services.fusiontables.FusiontablesRequest;
import com.google.api.services.fusiontables.FusiontablesScopes;
import com.google.api.services.fusiontables.model.Table;
import com.google.api.services.fusiontables.model.TableList;
*/
public class FusionTablesConnection {

	/*private JsonHttpRequestInitializer initializer;
	
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	private Fusiontables fusionTablesInstance;
	
	private static final String CLIENT_ID = "736466375713.apps.googleusercontent.com";
	private static final String EMAIL_ADDRESS = "736466375713@developer.gserviceaccount.com";

	private GoogleCredential credential;
	private Context context;
	private static final String API_KEY = "AIzaSyAGLdPN6MvRTmxXnlKp2OZsw-7RxjZa4o4";
	public FusionTablesConnection(Context context, String tableId) {
		this.context = context;
		
		createGoogleCredential();
		initializeFusionTables();
		
	}
	
	private void initializeFusionTables(){
		fusionTablesInstance = new Fusiontables.Builder(
				HTTP_TRANSPORT, JSON_FACTORY, credential)
				.setApplicationName("denuemariotest")
				.build();
	}
	
	private void createApiKeyInitializerCredential(){
		initializer = new GoogleKeyInitializer(FusionTablesConnection.API_KEY);
	}
	
	private void createGoogleCredential(){
		try {
			credential = new GoogleCredential.Builder().setTransport(HTTP_TRANSPORT)
					.setJsonFactory(JSON_FACTORY)
					.setServiceAccountId(EMAIL_ADDRESS)
					//.setServiceAccountUser(CLIENT_ID)
					.setServiceAccountScopes(FusiontablesScopes.FUSIONTABLES_READONLY)
					.setServiceAccountPrivateKeyFromP12File(getP12File())
					.build();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private File getP12File(){
		copyRawFileToDataLocation();
		AssetManager assetManager = context.getAssets();
		File dir = context.getFilesDir();
		File file = new File(dir, "denuetestkey.p12");
		return file;
	}
	
	private void copyRawFileToDataLocation(){
		InputStream ins = context.getResources().openRawResource(R.raw.denuetestkey);
		BufferedReader br = new BufferedReader(new InputStreamReader(ins));
		String line;
		try {
			FileOutputStream fOut =
					context.openFileOutput("denuetestkey.p12", Context.MODE_PRIVATE);
			OutputStreamWriter osw = new
					OutputStreamWriter(fOut);
			while((line = br.readLine()) != null){
				osw.write(line);
				osw.flush();
			}
			osw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Fusiontables getFusionTablesInstance(){
		return fusionTablesInstance;
	}

	public void listTables() throws IOException{
		header("Listing my Tables");
		Fusiontables.Table.List listTables = fusionTablesInstance.table().list();
		TableList tablelist = listTables.execute();
		if (tablelist.getItems() == null || tablelist.getItems().isEmpty()) {
		      System.out.println("No tables found!");
		      return;
		    }

		    for (Table table : tablelist.getItems()) {
		      show(table);
		      separator();
		    }
	}
	
	class FusiontablesRequestInitializer implements JsonHttpRequestInitializer {
	      public void initialize(JsonHttpRequest request) {
	        FusiontablesRequest fusiontablesRequest = (FusiontablesRequest)request;
	        fusiontablesRequest.setPrettyPrint(true);
	        fusiontablesRequest.setKey(FusionTablesConnection.API_KEY);
	    }
	}
	static void header(String name) {
	    System.out.println();
	    System.out.println("================== " + name + " ==================");
	    System.out.println();
	}
	
	static void show(Table table) {
	    System.out.println("id: " + table.getTableId());
	    System.out.println("name: " + table.getName());
	    System.out.println("description: " + table.getDescription());
	    System.out.println("attribution: " + table.getAttribution());
	    System.out.println("attribution link: " + table.getAttributionLink());
	    System.out.println("kind: " + table.getKind());

	}
	
	static void separator() {
	    System.out.println();
	    System.out.println("------------------------------------------------------");
	    System.out.println();
	}
	*/
}

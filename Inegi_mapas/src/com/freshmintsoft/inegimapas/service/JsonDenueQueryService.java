package com.freshmintsoft.inegimapas.service;



import android.app.Activity;

import com.freshmintsoft.inegimapas.DenueApplication;
import com.freshmintsoft.inegimapas.RespuestaInegiListener;


/**
 * Json implementation for the RespuestaInegi interface.
 * 
 * Not implemented in this version.
 * @author freshmintsoft
 *
 */
public class JsonDenueQueryService implements DenueQueryServiceInterface {

	public void getDataBasedOnGps() {

	}
	
	//No implementado en este release.
	public void queryServer(String query, String entidadOMunicipio) {
		
	}

	//No implementado en este release.
	public void queryServerWithDeviceLocation(String query) {
		
	}
	
	//No implementado en este release.
	public DenueApplication getApplication(){
		return null;
	}

	//No implementado en este release.
	public void setCallView(Activity activity) {
		
	}

	//No implementado en este release.
	public RespuestaInegiListener getListener(){
		return null;
	}
	
}

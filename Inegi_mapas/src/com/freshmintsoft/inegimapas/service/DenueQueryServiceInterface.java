package com.freshmintsoft.inegimapas.service;


import android.app.Activity;

import com.freshmintsoft.inegimapas.DenueApplication;
import com.freshmintsoft.inegimapas.RespuestaInegiListener;

/**
 * Interfaz que contiene los diferentes metodos de consulta
 *  a enviar al servicio del DENUE.
 * @author freshmintsoft
 * @see XmlDenueQueryService
 * @see JsonDenueQueryService
 */
public interface DenueQueryServiceInterface {

	public void getDataBasedOnGps();
	public void queryServer(String query, String entidadOMunicipio);
	public void queryServerWithDeviceLocation(String query);
	
	public DenueApplication getApplication();
	public void setCallView(Activity activity);
	public RespuestaInegiListener getListener();
}

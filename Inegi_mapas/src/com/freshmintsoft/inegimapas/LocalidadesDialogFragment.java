package com.freshmintsoft.inegimapas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;



import android.widget.ArrayAdapter;



public class LocalidadesDialogFragment extends DialogFragment{
		
	ArrayAdapter<String> aa;
	private String[] contentArray;
	DenueApplication application;
	
	public interface LocalidadesListener{
		public void onEstadoSelected();
	}
	
	LocalidadesListener listener;
	
	static LocalidadesDialogFragment newInstace(){
		LocalidadesDialogFragment d = new LocalidadesDialogFragment();
		Bundle args = new Bundle();
		d.setArguments(args);
		
		return d;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		application = (DenueApplication) getActivity().getApplication();
		listener = (LocalidadesListener) getActivity();
	}

	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);
		contentArray = getResources().getStringArray(R.array.localidades_array);
		aa = new ArrayAdapter<String>(getActivity(), 
											android.R.layout.simple_list_item_1, 
											contentArray);
		return new AlertDialog.Builder(getActivity())
		.setIcon(R.drawable.device_map_overlay)
		.setTitle("Selecciona el Estado")
		.setAdapter(aa, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				application.localidad = which;
				listener.onEstadoSelected();
			}
		})
		.create();
	}
}

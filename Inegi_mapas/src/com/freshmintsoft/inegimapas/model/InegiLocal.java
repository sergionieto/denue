package com.freshmintsoft.inegimapas.model;

/**
 * Representacion de la informacion obtenida en el
 * Web Service del DENUE.
 * 
 * @author freshmintsoft
 * @see DenueConnectionService
 * @see DenueQueryServiceInterface
 * @see DenueSoapResponseUnMarshaller
 */
public class InegiLocal {

	private Integer idDato;
	private String nombreUnidadEconomica;
	private String razonSocial;
	private String codigoClase;
	private String nombreClase;
	private String personalOcupado;
	private String tipoVialidad;
	private String nombreVialidad;
	private String numeroExterior;
	private String edificio;
	private String numeroInterior;
	private String nombreAsentamiento;
	private String corredorIndustrial;
	private String numeroLocal;
	private String codigoPostal;
	private String entidadFederativa;
	private String localidad;
	private String areaGeoestadistica;
	private String manzana;
	private String telefono;
	private String email;
	private String website;
	private String tipoUnidadEconomica;
	private String latitud;
	private String longitud;
	private String fechaIncorporacion;
	
	public String toString(){
		return nombreUnidadEconomica + ", " + entidadFederativa + ", " + localidad;
	}
	
	public Integer getIdDato() {
		return idDato;
	}
	public void setIdDato(Integer idDato) {
		this.idDato = idDato;
	}
	public String getNombreUnidadEconomica() {
		return nombreUnidadEconomica;
	}
	public void setNombreUnidadEconomica(String nombreUnidadEconomica) {
		this.nombreUnidadEconomica = nombreUnidadEconomica;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getCodigoClase() {
		return codigoClase;
	}
	public void setCodigoClase(String codigoClase) {
		this.codigoClase = codigoClase;
	}
	public String getNombreClase() {
		return nombreClase;
	}
	public void setNombreClase(String nombreClase) {
		this.nombreClase = nombreClase;
	}
	public String getPersonalOcupado() {
		return personalOcupado;
	}
	public void setPersonalOcupado(String personalOcupado) {
		this.personalOcupado = personalOcupado;
	}
	public String getTipoVialidad() {
		return tipoVialidad;
	}
	public void setTipoVialidad(String tipoVialidad) {
		this.tipoVialidad = tipoVialidad;
	}
	public String getNombreVialidad() {
		return nombreVialidad;
	}
	public void setNombreVialidad(String nombreVialidad) {
		this.nombreVialidad = nombreVialidad;
	}
	public String getNumeroExterior() {
		return numeroExterior;
	}
	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}
	public String getEdificio() {
		return edificio;
	}
	public void setEdificio(String edificio) {
		this.edificio = edificio;
	}
	public String getNumeroInterior() {
		return numeroInterior;
	}
	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}
	public String getNombreAsentamiento() {
		return nombreAsentamiento;
	}
	public void setNombreAsentamiento(String nombreAsentamiento) {
		this.nombreAsentamiento = nombreAsentamiento;
	}
	public String getCorredorIndustrial() {
		return corredorIndustrial;
	}
	public void setCorredorIndustrial(String corredorIndustrial) {
		this.corredorIndustrial = corredorIndustrial;
	}
	public String getNumeroLocal() {
		return numeroLocal;
	}
	public void setNumeroLocal(String numeroLocal) {
		this.numeroLocal = numeroLocal;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getEntidadFederativa() {
		return entidadFederativa;
	}
	public void setEntidadFederativa(String entidadFederativa) {
		this.entidadFederativa = entidadFederativa;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getAreaGeoestadistica() {
		return areaGeoestadistica;
	}
	public void setAreaGeoestadistica(String areaGeoestadistica) {
		this.areaGeoestadistica = areaGeoestadistica;
	}
	public String getManzana() {
		return manzana;
	}
	public void setManzana(String manzana) {
		this.manzana = manzana;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getTipoUnidadEconomica() {
		return tipoUnidadEconomica;
	}
	public void setTipoUnidadEconomica(String tipoUnidadEconomica) {
		this.tipoUnidadEconomica = tipoUnidadEconomica;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getFechaIncorporacion() {
		return fechaIncorporacion;
	}
	public void setFechaIncorporacion(String fechaIncorporacion) {
		this.fechaIncorporacion = fechaIncorporacion;
	}
	
	
	
}

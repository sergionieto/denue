package com.freshmintsoft.inegimapas;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;

import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class TabMapsListActivity extends TabActivity{

	DenueApplication app;
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_map_list);
        
        app = (DenueApplication) getApplication();
        TabHost tabHost = getTabHost();
        
        //Tab de la lista
        TabSpec listaTab = tabHost.newTabSpec("Lista");
        listaTab.setIndicator("Lista");
        Intent listaIntent = new Intent(this, ResultadosListActivity.class);
        listaTab.setContent(listaIntent);
        
        //Tab de Mapas
        TabSpec mapTab = tabHost.newTabSpec("Mapa");
        mapTab.setIndicator("Mapa");
        Intent mapIntent = new Intent(this, GoogleMapsListActivity.class);
        mapTab.setContent(mapIntent);
        
        
        tabHost.addTab(listaTab);
        tabHost.addTab(mapTab);
        
        tabHost.setCurrentTab(1);
	}
}

package com.freshmintsoft.inegimapas;




import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ToggleButton;

/**
 * Super clase que setea el menu de preferencias.
 * y responde a eventos de dicho menu.
 * @author freshmintsoft
 *
 */
public abstract class DenueActivity extends FragmentActivity{
	int index = -1;
	public static final String LIST_INDEX_KEY = "lastIndexKey"; //Referencia del indice de la lista de resultados en ResultadosListActivity
	ProgressDialog progressDialog;
	ToggleButton toggleButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	public void connectToService(){}
	
	public void showProgressDialog(){
		lockOrientation();
		progressDialog = ProgressDialog.show(
				this, "Conectado al Servidor", "Por favor espera");
	}
	
	public void lockOrientation(){
		int currentOrientation = getResources().getConfiguration().orientation;
		if(currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
	}
	
	public void unlockScreenOrientation(){
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	private void CreateMenu(Menu menu){
		MenuItem preferences = menu.add(0,0,0, "Preferencias");
		{
			//preferences.setIcon(R.drawable.menu_mapa);
			//mapDisplay.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		return MenuChoice(item);
	}

	private boolean MenuChoice(MenuItem item){
		switch(item.getItemId()){
			case 0:
				Intent preference = new Intent(this, DenuePreferences.class);
				startActivity(preference);
				return true;
			case 1:
				Intent googleMaps = new Intent(this, GoogleMapsListActivity.class);
				googleMaps.putExtra(LIST_INDEX_KEY, index);
				startActivity(googleMaps);
				return true;
		}
		return false;
	}
}

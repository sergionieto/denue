//
//  Datos.m
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import "Datos.h"

@implementation Datos

@synthesize idDato;
@synthesize nombreUnidadEconomica;
@synthesize razonSocial;
@synthesize codigoClase;
@synthesize nombreClase;
@synthesize personalOcupado;
@synthesize tipoVialidad;
@synthesize nombreVialidad;
@synthesize numeroExterior;
@synthesize edificio;
@synthesize numeroInterior;
@synthesize nombreAsentamiento;
@synthesize corredorIndustrial;
@synthesize numeroLocal;
@synthesize codigoPostal;
@synthesize entidadFederativa;
@synthesize municipio;
@synthesize localidad;
@synthesize areaGeoestadistica;
@synthesize manzana;
@synthesize telefono;
@synthesize email;
@synthesize website;
@synthesize tipoUnidadEconomica;
@synthesize latitud;
@synthesize longitud;
@synthesize fechaIncorporacion;

-(id)init
{
    if(self = [super init])
    {
	}
	return self;
}

@end

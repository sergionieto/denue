//
//  MapViewController.h
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "GetDatos.h"

#import "KMLParser.h"

@class DetailViewController;

@interface MapViewController : UIViewController<MKMapViewDelegate, GetDatosDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>
{
    KMLParser *kmlParser;
    id delegate;
    AppDelegate *appDelegate;
    DetailViewController *detailViewController;
    GetDatos *getDatos;
}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSString *objetoSeleccionado;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) UIPopoverController *popResultados;

-(void)changeMapType;

@end

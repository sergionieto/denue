//
//  AppDelegate.m
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 10/27/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "MapViewController.h"

@implementation AppDelegate
@synthesize arrayInfo, tipoBusqueda, userLatitude, userLongitude, tipoCategoria, query, topeQ;
@synthesize maxLatitude, maxLongitude;
@synthesize minLatitude, minLongitude;
@synthesize radioBusqueda, id_entidad, id_localidad, id_categoria;
@synthesize nameEntidad, nameLocalidad, statusGPS;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //init arrayInfo
    arrayInfo = [[NSMutableArray alloc] init];
    topeQ = @"50";
    statusGPS = @"YES";
    radioBusqueda = @"500";

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil];
    } else {
        self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    //navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    //navigationController.navigationBar.opaque = YES;
    navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.12 green:0.24 blue:0.52 alpha:1.0];
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    return YES;
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

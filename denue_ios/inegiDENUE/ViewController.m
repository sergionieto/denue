//
//  ViewController.m
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 10/27/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import "ViewController.h"
#import "MapViewController.h"
#import "ResultadosViewController.h"
#import "CategoriaViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize queryText;
@synthesize backgroundImage;
@synthesize tabBarController;
@synthesize locationManager;
@synthesize buttonInfo;

#pragma mark - Métodos

-(void)searchQuery
{
    if (appDelegate.tipoBusqueda == NULL)
    {
        appDelegate.tipoBusqueda = @"1";
        appDelegate.radioBusqueda = @"500";
    }
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
    
    mapViewController = [[MapViewController alloc]
                         initWithNibName:@"MapViewController" bundle:nil];
    resultadosView = [[ResultadosViewController alloc] initWithStyle:UITableViewStyleGrouped];
    
    tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = @[mapViewController, resultadosView];
    [self.navigationController pushViewController:tabBarController animated:YES];
    }else
    {
    mapViewController = [[MapViewController alloc]
                             initWithNibName:@"MapViewController_iPad" bundle:nil];
    resultadosView = [[ResultadosViewController alloc] initWithStyle:UITableViewStyleGrouped];
        
    tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = @[mapViewController, resultadosView];
    [self.navigationController pushViewController:tabBarController animated:YES];
    //[self.navigationController pushViewController:mapViewController animated:YES];
    }
     
}

-(IBAction)goInfo:(id)sender
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        configuracionView = [[ConfiguracionViewController alloc] initWithStyle:UITableViewStyleGrouped];
        [self.navigationController pushViewController:configuracionView animated:YES];
    }else{
        configuracionView = [[ConfiguracionViewController alloc] initWithStyle:UITableViewStyleGrouped];
        [self.navigationController pushViewController:configuracionView animated:YES];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    appDelegate.query = queryText.text;
    [textField resignFirstResponder];
    queryText.text = @"";
    [self searchQuery];
    return YES;
}

-(void)goCategoria
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        
        categoriaView = [[CategoriaViewController alloc] initWithNibName:@"CategoriaViewController_iPhone" bundle:nil];
    }else
    {
        categoriaView = [[CategoriaViewController alloc] initWithNibName:@"CategoriaViewController_iPad" bundle:nil];
    }
    
    [self.navigationController pushViewController:categoriaView animated:YES];
}

-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    CLLocationCoordinate2D here =  newLocation.coordinate;
    appDelegate.userLatitude = [NSString stringWithFormat:@"%f",here.latitude];
    appDelegate.userLongitude = [NSString stringWithFormat:@"%f",here.longitude];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //user location
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    NSLog(@"%@ - %@",appDelegate.userLatitude, appDelegate.userLongitude);
    
    
    
    //Validacion envio de datos
    NSData *dataValidation = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.google.com"]];
    if(dataValidation == NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Advertencia" message:@"El dispositivo no cuenta con conexión a internet." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [queryText setEnabled:NO];
        [buttonInfo setEnabled:NO];
        [buttonInfo setHidden:YES];
        
    }else{
        appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        UIBarButtonItem *buttonCategoria = [[UIBarButtonItem alloc] initWithTitle:@"Categoría" style:UIBarButtonItemStylePlain target:self action:@selector(goCategoria)];
        self.navigationItem.rightBarButtonItem = buttonCategoria;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
            [[NSNotificationCenter defaultCenter]
             addObserver:self selector:@selector(orientationChanged:)
             name:UIDeviceOrientationDidChangeNotification
             object:[UIDevice currentDevice]];
        }
    }
	// Do any additional setup after loading the view, typically from a nib.
}

- (void) orientationChanged:(NSNotification *)note
{
    /*
    UIDevice * device = note.object;
    if ((device.orientation == UIDeviceOrientationLandscapeLeft) || (device.orientation == UIDeviceOrientationLandscapeRight))
    {
        backgroundImage.frame = CGRectMake(0, 0, 1024, 704);
        backgroundImage.contentMode = UIViewContentModeScaleToFill;
        [backgroundImage setImage:[UIImage imageNamed:@"mainMenu_iPad_h"]];
    }else if ((device.orientation == UIDeviceOrientationPortrait) || (device.orientation == UIDeviceOrientationPortraitUpsideDown))
    {
        backgroundImage.frame = CGRectMake(0, 0, 768, 960);
        backgroundImage.contentMode = UIViewContentModeScaleToFill;
        backgroundImage.image = [UIImage imageNamed:@"mainMenu_iPad"];
    }
     */
    NSLog(@"notification");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        return (interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    }else
    {
        return YES;
    }
}

@end

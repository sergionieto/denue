//
//  AppDelegate.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 10/27/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) NSMutableArray *arrayInfo;
@property (strong, nonatomic) NSString *tipoBusqueda;
@property (strong, nonatomic) NSString *userLongitude;
@property (strong, nonatomic) NSString *userLatitude;
@property (strong, nonatomic) NSString *maxLongitude;
@property (strong, nonatomic) NSString *maxLatitude;
@property (strong, nonatomic) NSString *minLongitude;
@property (strong, nonatomic) NSString *minLatitude;
@property (strong, nonatomic) NSString *tipoCategoria;
@property (strong, nonatomic) NSString *query;
@property (strong, nonatomic) NSString *topeQ;
@property (strong, nonatomic) NSString *radioBusqueda;
@property (strong, nonatomic) NSString *id_entidad;
@property (strong, nonatomic) NSString *id_localidad;
@property (strong, nonatomic) NSString *id_categoria;
@property (strong, nonatomic) NSString *nameEntidad;
@property (strong, nonatomic) NSString *nameLocalidad;
@property (strong, nonatomic) NSString *statusGPS;

@end

//
//  MunicipioViewController.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/21/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Municipio.h"

@interface MunicipioViewController : UITableViewController
{
    AppDelegate *appDelegate;
}

@property (strong, nonatomic) NSMutableArray *arrayMunicipios;
@property (strong, nonatomic) NSString *responseJSON;
@property (strong, nonatomic) NSURLConnection *connectionInProgress;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSMutableData *jsonData;

-(NSString*)getPath;




@end

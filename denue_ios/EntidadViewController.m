//
//  EntidadViewController.m
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/20/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import "EntidadViewController.h"

@interface EntidadViewController ()

@end

@implementation EntidadViewController

@synthesize arrayEntidades, responseJSON, connectionInProgress, url, jsonData;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"Entidad";
    }
    return self;
}

-(NSString*)getPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"entidad.json"];
    
    return path;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"%@",[self getPath]);
    
    //Make the information array
    
    url = [NSURL URLWithString:@"https://dl.dropbox.com/u/2182160/entidad.json"];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    
    connectionInProgress = [[NSURLConnection alloc] initWithRequest:req delegate:self];
    if (connectionInProgress)
    {
        jsonData = [[NSMutableData alloc] init];
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [arrayEntidades count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    // Configure the cell...
    
    Entidad *entidad = [arrayEntidades objectAtIndex:indexPath.row];
    cell.textLabel.text = entidad.descripcionEntidad;
    
    return cell;
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [jsonData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSError *error;
    
    responseJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [responseJSON writeToFile:[self getPath] atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    NSLog(@"** %@",responseJSON);
    
    NSArray *json = [NSJSONSerialization
                     JSONObjectWithData:[NSData dataWithContentsOfFile:[self getPath]]
                     options:kNilOptions
                     error:&error];
    
    [arrayEntidades removeAllObjects];
    
    arrayEntidades = [[NSMutableArray alloc] init];
    
    for (int x = 0; x<[json count]; x++)
    {
        //init PreguntasAsesor
        Entidad *entidad = [[Entidad alloc] init];
        
        //init Dato Dictionary
        NSDictionary *dato = [json objectAtIndex:x];
        
        entidad.idEntidad = [dato objectForKey:@"ENTIDAD_ID"];
        entidad.descripcionEntidad = [dato objectForKey:@"DES_ENTIDAD"];
        
        [arrayEntidades addObject:entidad];
    }
    
    NSLog(@"%i",[arrayEntidades count]);
    
    [self.tableView reloadData];

}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error");
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    Entidad *entidad = [arrayEntidades objectAtIndex:indexPath.row];
    appDelegate.id_entidad = entidad.idEntidad;
    appDelegate.nameEntidad = entidad.descripcionEntidad;
    
    municipioView = [[MunicipioViewController alloc] initWithStyle:UITableViewStyleGrouped];
    
    [self.navigationController pushViewController:municipioView animated:YES];
    
}



@end

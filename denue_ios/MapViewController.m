//
//  MapViewController.m
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import "MapViewController.h"
#import "Datos.h"
#import "DetailViewController.h"
#import "ResultadosViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController
@synthesize mapView;
@synthesize objetoSeleccionado;
@synthesize activityIndicator;
@synthesize locationManager;
@synthesize popResultados;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.image = [UIImage imageNamed:@"0063.png"];
        self.title = @"Mapa";
    }
    return self;
}

#pragma mark - Draw Annotations


#pragma mark - Change Map

-(void)changeMapType
{
    if (self.mapView.mapType == MKMapTypeStandard){
        self.mapView.mapType = MKMapTypeSatellite;
    }else if( self.mapView.mapType == MKMapTypeSatellite){
        self.mapView.mapType = MKMapTypeHybrid;
    }else{
        self.mapView.mapType = MKMapTypeStandard;
    }
}

-(void)changeList
{
    ResultadosViewController *resultadosView = [[ResultadosViewController alloc] initWithStyle:UITableViewStyleGrouped];
    [self.navigationController pushViewController:resultadosView animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)//OK button pressed
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - GetDatos Delegate

-(void)returnResponse
{
    NSLog(@"Finish Loading");

    [activityIndicator stopAnimating];
    
    
    if (([appDelegate.arrayInfo count] == 0)) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Advertencia" message:@"La búsqueda realizada no tuvo ningún resultado." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }else{
        Datos *dato = [appDelegate.arrayInfo objectAtIndex:0];
        if([dato.idDato isEqualToString:@"Error"]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Advertencia" message:@"La búsqueda realizada no tuvo ningún resultado." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }else{
            for (int x = 0; x<[appDelegate.arrayInfo count]; x++) {
                
                Datos *datos = [appDelegate.arrayInfo objectAtIndex:x];
                
                CLLocationCoordinate2D annotationCoord;
                
                annotationCoord.latitude = [datos.latitud floatValue];
                annotationCoord.longitude = [datos.longitud floatValue];
                
                MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
                
                annotationPoint.coordinate = annotationCoord;
                annotationPoint.title = [NSString stringWithFormat:@"%i",x];
                annotationPoint.subtitle = [NSString stringWithFormat:@"%@ %@ %@ %@",datos.nombreUnidadEconomica, datos.nombreVialidad, datos.numeroExterior, datos.nombreAsentamiento];
                
                [mapView addAnnotation:annotationPoint];
                
                if (x == 0) {
                    appDelegate.maxLatitude = [NSString stringWithFormat:@"%f",[datos.latitud floatValue]];
                    appDelegate.maxLongitude = [NSString stringWithFormat:@"%f",[datos.longitud floatValue]];
                    appDelegate.minLatitude = [NSString stringWithFormat:@"%f",[datos.latitud floatValue]];
                    appDelegate.minLongitude = [NSString stringWithFormat:@"%f",[datos.longitud floatValue]];
                }else{
                    if ([datos.latitud floatValue] >= [appDelegate.maxLatitude floatValue]) {
                        appDelegate.maxLatitude = datos.latitud;
                    }
                    if ([datos.longitud floatValue] >= [appDelegate.maxLongitude floatValue]) {
                        appDelegate.maxLongitude = datos.longitud;
                    }
                    if ([datos.latitud floatValue] < [appDelegate.minLatitude floatValue]) {
                        appDelegate.minLatitude = datos.latitud;
                    }
                    if ([datos.longitud floatValue] < [appDelegate.minLongitude floatValue]) {
                        appDelegate.minLongitude = datos.longitud;
                    }
                    
                }
                
                CLLocationCoordinate2D annotationRegionMax;
                CLLocationCoordinate2D annotationRegionMin;
                
                annotationRegionMax.latitude = [appDelegate.maxLatitude floatValue];
                annotationRegionMax.longitude = [appDelegate.maxLongitude floatValue];
                
                annotationRegionMin.latitude = [appDelegate.minLatitude floatValue];
                annotationRegionMin.longitude = [appDelegate.minLongitude floatValue];
                
                float ancho = [appDelegate.maxLatitude floatValue] - [appDelegate.minLatitude floatValue];
                float alto = [appDelegate.maxLongitude floatValue] - [appDelegate.minLongitude floatValue];
                
                NSLog(@"minLat: %f \n minLong: %f \n ancho: %f \n alto: %f",[appDelegate.minLatitude floatValue],[appDelegate.minLongitude floatValue],[appDelegate.maxLatitude floatValue], [appDelegate.maxLongitude floatValue]);
                ;
                
                MKCoordinateSpan regionSpan = MKCoordinateSpanMake(ancho*2,alto*2);
                MKCoordinateRegion region = MKCoordinateRegionMake(annotationRegionMin, regionSpan);
                
                [mapView setRegion:region animated:YES];
                
            }
        }
    }
    
    
    
    
    
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
    
    // Locate the path to the route.kml file in the application's bundle
    // and parse it with the KMLParser.
    
    /*
    
     
    NSLog(@"Done Parse");
    NSLog(@"Número de Overlays: %i",[[kmlParser overlays] count]);
    
    // Add all of the MKOverlay objects parsed from the KML file to the map.
    
    */
    
    
    // * * * * * * * * * * * * * * * * * * * * * * * * *
    // * Para pintar las manzanas por favor            *
    // * quitar el comentario a esta sección.          *
    // * * * * * * * * * * * * * * * * * * * * * * * * *
    
    /*
    NSString *path = [[NSBundle mainBundle] pathForResource:@"regiones" ofType:@"kml"];
    
    NSURL *url = [NSURL fileURLWithPath:path];
    
    kmlParser = [[KMLParser alloc] initWithURL:url];
    [kmlParser parseKML];
    
    //Agregar las capas en un solo paso
    [mapView addOverlays:[kmlParser overlays]];
    
     */
    //Agregar las capas una por una
//    for (int x=0; x<[[kmlParser overlays] count]; x++) {
//        NSLog(@"overlay: %i",x);
//        [mapView addOverlay:[[kmlParser overlays] objectAtIndex:x]];
//    }
//    
    
     // * * * * * * * * * * * * * * * * * * * * * * * * *
     // * Para pintar las manzanas por favor            *
     // * quitar el comentario a esta sección.          *
     // * * * * * * * * * * * * * * * * * * * * * * * * *
     
     
      
    
    // Add all of the MKAnnotation objects parsed from the KML file to the map.
    //annotations = [[NSArray alloc] initWithArray:[kmlParser points]];
    //[mapView addAnnotations:annotations];
     
    

   // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
    
    //Zoom to user location
    
    // Walk the list of overlays and annotations and create a MKMapRect that
    // bounds all of them and store it into flyTo.
    
    /*
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKOverlay> overlay in overlays) {
        if (MKMapRectIsNull(flyTo)) {
            flyTo = [overlay boundingMapRect];
        } else {
            flyTo = MKMapRectUnion(flyTo, [overlay boundingMapRect]);
        }
    }
    */
    /*
    for (id <MKAnnotation> annotation in annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        } else {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
     */
    
    // Position the map so that all overlays and annotations are visible on screen.
    //mapView.visibleMapRect = flyTo;
    
    /*
    // User location
    
     CLLocationCoordinate2D annotationCoord;
     
     annotationCoord.latitude = [appDelegate.userLatitude floatValue];
     annotationCoord.longitude = [appDelegate.userLongitude floatValue];
     
     MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(annotationCoord, 5000, 5000);
     
     [mapView setRegion:region animated:YES];
    */
     
}

-(void)mapView:(MKMapView *)mapView didAddOverlayViews:(NSArray *)overlayViews
{
    //NSLog(@" * * * * * * * * * * * * ");
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
    MKPointAnnotation *annotationPoint = view.annotation;
    
    if (![annotationPoint.title isEqualToString:@"Current Location"])
    {
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        rightButton.tag = [annotationPoint.title intValue];
        
        [rightButton addTarget:self action:@selector(showDetails:) forControlEvents:UIControlEventTouchUpInside];
        
        view.rightCalloutAccessoryView = rightButton;
    }
    
}

-(void)showDetails:(id)annotationClicked
{
    NSLog(@"Annotation button clicked %i \n %@",[annotationClicked tag], annotationClicked);
    
    detailViewController = [[DetailViewController alloc] initWithStyle:UITableViewStyleGrouped];
    detailViewController.objetoSeleccionado = [NSString stringWithFormat:@"%i",[annotationClicked tag]];
    
    [self.navigationController pushViewController:detailViewController animated:YES];
}
 
-(void)showList
{
    if ([popResultados isPopoverVisible]) {
        [popResultados dismissPopoverAnimated:YES];
    }else
    {
        [popResultados presentPopoverFromBarButtonItem:[self.navigationItem.rightBarButtonItems objectAtIndex:0] permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

/*
-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    CLLocationCoordinate2D here =  newLocation.coordinate;
    appDelegate.userLatitude = [NSString stringWithFormat:@"%f",here.latitude];
    appDelegate.userLongitude = [NSString stringWithFormat:@"%f",here.longitude];
}
 */

#pragma mark MKMapViewDelegate

/*
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    return [kmlParser viewForOverlay:overlay];
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    return [kmlParser viewForAnnotation:annotation];
}
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Add button to change the mapType

    //Remove All Annotations
    
    NSMutableArray *annotationsToRemove = [[NSMutableArray alloc] initWithArray:mapView.annotations];
    
    [annotationsToRemove removeObject:mapView.userLocation];
    
    [mapView removeAnnotations:annotationsToRemove];
    
        
        if ([CLLocationManager locationServicesEnabled])
        {
            NSLog(@"Activate");
        }
    
        //user location
        mapView.showsUserLocation = YES;

        appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        //init UIActivityIndicator
        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [activityIndicator hidesWhenStopped];
        [activityIndicator startAnimating];
        
        //Add activityButton
        UIBarButtonItem *activityButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            UIBarButtonItem *buttonChangeMap = [[UIBarButtonItem alloc] initWithTitle:@"Cambiar Mapa" style:UIBarButtonItemStylePlain target:self action:@selector(changeMapType)];
            UIBarButtonItem *buttonList = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(changeList)];
            NSArray *arrayButtons = [[NSArray alloc] initWithObjects:buttonList, buttonChangeMap, activityButton, nil];
            self.navigationItem.rightBarButtonItems = arrayButtons;
        }else
        {
            ResultadosViewController *resultadosView = [[ResultadosViewController alloc] initWithStyle:UITableViewStyleGrouped];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:resultadosView];
            
            UIBarButtonItem *buttonChangeMap = [[UIBarButtonItem alloc] initWithTitle:@"Cambiar Mapa" style:UIBarButtonItemStylePlain target:self action:@selector(changeMapType)];
            UIBarButtonItem *buttonList = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(showList)];
            NSArray *arrayButtons = [[NSArray alloc] initWithObjects:buttonList, buttonChangeMap, activityButton, nil];
            self.navigationItem.rightBarButtonItems = arrayButtons;
           
            popResultados = [[UIPopoverController alloc] initWithContentViewController:navigationController];
            popResultados.popoverContentSize = CGSizeMake(320, 480);
             
        }
    
        //User Location

        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
    
    NSLog(@"%@ - %@",appDelegate.userLatitude, appDelegate.userLongitude);
    NSLog(@"Get Datos");

        //init GetDatos
        
        getDatos = [[GetDatos alloc] init];
        getDatos.delegate = self;
        [getDatos startConnection];

    self.mapView.mapType = MKMapTypeHybrid;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

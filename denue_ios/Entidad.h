//
//  Entidad.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/20/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Entidad : NSObject

@property (strong, nonatomic) NSString *idEntidad;
@property (strong, nonatomic) NSString *descripcionEntidad;

@end

//
//  ConfiguracionViewController.m
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/25/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import "ConfiguracionViewController.h"

@interface ConfiguracionViewController ()

@end

@implementation ConfiguracionViewController
@synthesize buttonSwitch, segmentedControl;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"Configuración";
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //Radio de busqueda
    appDelegate.radioBusqueda = @"500";
    
    //Número de búsqueda
    appDelegate.topeQ = @"5";
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

-(void)changeGPS
{
    if ([buttonSwitch isOn]) {
        NSLog(@"ON");
        appDelegate.tipoBusqueda = @"1";
        appDelegate.statusGPS = @"YES";
        [self.tableView reloadData];
    }else{
        NSLog(@"OFF");
        appDelegate.tipoBusqueda = @"2";
        appDelegate.statusGPS = @"NO";
    }
}

-(void)selectRadio
{
    if ([segmentedControl selectedSegmentIndex] == 0) {
        appDelegate.radioBusqueda = @"500";
    }
    if ([segmentedControl selectedSegmentIndex] == 1) {
        appDelegate.radioBusqueda = @"1000";
    }
    if ([segmentedControl selectedSegmentIndex] == 2) {
        appDelegate.radioBusqueda = @"5000";
    }
    if ([segmentedControl selectedSegmentIndex] == 3) {
        appDelegate.radioBusqueda = @"10000";
    }
    
    NSLog(@"%@",appDelegate.radioBusqueda);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    
    // Configure the cell...
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Número de resultados";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",appDelegate.topeQ];
    }
    if (indexPath.row == 1) {
        
        //init buttonSwitch
        buttonSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        
        //configure buttonSwitch
        //configure GPS
        if ([appDelegate.statusGPS isEqualToString:@"YES"]) {
            [buttonSwitch setOn:YES animated:YES];
        }else if ([appDelegate.statusGPS isEqualToString:@"NO"])
        {
            [buttonSwitch setOn:NO animated:YES];
        }
        
        [buttonSwitch addTarget:self action:@selector(changeGPS) forControlEvents:UIControlEventValueChanged];
        
        [cell addSubview:buttonSwitch];
        cell.accessoryView = buttonSwitch;
        cell.textLabel.text = @"GPS";
        
    }
    if (indexPath.row == 2) {

        segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"500",@"1000",@"5000",@"10000", nil]];
        [segmentedControl addTarget:self action:@selector(selectRadio) forControlEvents:UIControlEventValueChanged];
        [segmentedControl setSelectedSegmentIndex:0];
        
        if ([appDelegate.radioBusqueda isEqualToString:@"500"]) {
            [segmentedControl setSelectedSegmentIndex:0];
        }
        if ([appDelegate.radioBusqueda isEqualToString:@"1000"]) {
            [segmentedControl setSelectedSegmentIndex:1];
        }
        if ([appDelegate.radioBusqueda isEqualToString:@"5000"]) {
            [segmentedControl setSelectedSegmentIndex:2];
        }
        if ([appDelegate.radioBusqueda isEqualToString:@"10000"]) {
            [segmentedControl setSelectedSegmentIndex:3];
        }
        
        [cell addSubview:segmentedControl];
        cell.accessoryView = segmentedControl;

        //cell.textLabel.numberOfLines = 3;
        //cell.textLabel.text = @"\nRadio de búsqueda.";
        //cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[arrayOpciones objectAtIndex:indexPath.row]];

    }
    if (indexPath.row == 3) {
        cell.textLabel.text = @"Seleccionar Entidad";
        cell.detailTextLabel.numberOfLines = 3;
        
        if ([appDelegate.statusGPS isEqualToString:@"YES"]) {
            cell.detailTextLabel.text = @"";
        }else{
            if ((appDelegate.nameEntidad == NULL) && (appDelegate.nameLocalidad == NULL)) {
                cell.detailTextLabel.text = @"";
            }else{
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@",appDelegate.nameEntidad, appDelegate.nameLocalidad];
            }
        }
        
    }
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if (indexPath.row == 0) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            infoView = [[InfoViewController alloc] initWithNibName:@"infoView_iPhone" bundle:nil];
        }else{
            infoView = [[InfoViewController alloc] initWithNibName:@"infoView_iPad" bundle:nil];
            }
        [self.navigationController pushViewController:infoView animated:YES];
        
    }
    if (indexPath.row == 1) {
        
                
    }
    if (indexPath.row == 2) {
        
    }
    if (indexPath.row == 3) {
        entidadView = [[EntidadViewController alloc] initWithStyle:UITableViewStyleGrouped];
        [self.navigationController pushViewController:entidadView animated:YES];
    }
}

@end

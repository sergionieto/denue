//
//  DetailViewController.h
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UITableViewController
{
    AppDelegate *appDelegate;
}

@property (strong, nonatomic) NSString *objetoSeleccionado;

@end

//
//  Municipio.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/21/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Municipio : NSObject

@property (strong, nonatomic) NSString *idEntidad;
@property (strong, nonatomic) NSString *idMunicipio;
@property (strong, nonatomic) NSString *descripcionMunicipio;

@end
